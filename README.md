# heysummit

## Caveats

1. Some aspects of this code base are incomplete due to time constraints
  - For example, rounded border for current day highlight
2. Some assumptions were made due to nature of exercise
  - For example, highlighted day can be both current or hovered, I chose current.
3. Not sure what's the green bar at the bottom
4. Parts of this are tied to the business rational, the whole logic, and the structure of the project itself, therefore are inevitably different than how they would be if coded in context.
  - For example, types are approximations according to the design
  - Design/style is sometimes not composable or reusable
5. Due to only having the screenshot some design elements are either missing or inaccurate in comparison to having access to Figma.
  - For example, fonts are off, shadows missing

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
