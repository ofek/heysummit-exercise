import { EventOrigin } from '@/types/EventOrigin';

export const getSourceIcon = (source: EventOrigin) => {
  switch (source) {
    case EventOrigin.FACEBOOK:
      return 'facebook-square';
    case EventOrigin.LINKEDIN:
      return 'linkedin';
    case EventOrigin.TWITTER:
      return 'twitter-square';
  }
};
