const months = [
  'Jan',
  'Feb',
  'Mar',
  'Apr',
  'May',
  'Jun',
  'Jul',
  'Aug',
  'Sep',
  'Oct',
  'Nov',
  'Dec',
];

export const prettyDate = (date: Date) => {
  return `${date.getDate()} ${months[date.getMonth()]}, ${date.getFullYear()}`;
};

export const prettyTime = (date: Date) => {
  return date
    .toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' })
    .toLowerCase()
    .split(' ')
    .join('');
};
