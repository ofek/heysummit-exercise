import { EventOrigin } from '@/types/EventOrigin';

export type DailySummary = {
  date: Date;
  name: string;
  latestTimestamp: Date;
  count: number;
  source: EventOrigin;
  isDone: boolean;
};
