import { DailySummary } from './DailySummary';

export type Blast = {
  lengthInDays: number;
  startDate: Date;
  summaries: DailySummary[][];
};
