export default class Event {
  name: string;
  source: EventOrigin;
  timestamp: Date;
  type: EventType;

  constructor(event: Partial<Event>) {
    Object.assign(this, event);
  }
}
