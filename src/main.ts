import { createApp } from 'vue';
import App from './App.vue';
import store from './store';

import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faClock,
  faCheckCircle,
  faMousePointer,
} from '@fortawesome/free-solid-svg-icons';
import {
  faFacebookSquare,
  faLinkedin,
  faTwitterSquare,
} from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

library.add(
  faClock,
  faCheckCircle,
  faMousePointer,
  faFacebookSquare,
  faLinkedin,
  faTwitterSquare
);

createApp(App)
  .use(store)
  .component('font-awesome-icon', FontAwesomeIcon)
  .mount('#app');
